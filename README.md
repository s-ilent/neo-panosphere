# Neo Panosphere

A panosphere shader with handy options and bug fixes.

VRchat comes with a panosphere shader, but it's a bit lacking in features. This panosphere shader is designed to let you set up complicated skyboxes that can be animated.

![](https://files.catbox.moe/d0vqd5.jpg)

## Options

* **Scroll**, when set, the scale/offset is used to set scroll values. 
* **Overlay** multiplies the overlay over the main texture, designed for masking regions out.
* **Sky and Ground Cover** projects the main texture across the sky/ground. 
* **Stereo Projection** treats the main texture as a top-bottom 3D image.
* **Infinite Projection** displays the material behind all other objects, like a skybox.