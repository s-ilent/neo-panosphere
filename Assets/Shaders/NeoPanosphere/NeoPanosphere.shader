// Made with Amplify's help.
Shader "Silent/NeoPanosphere"
{
	Properties
	{
		[Header(Main Texture)]
		[HDR][NoScaleOffset]_MainTex("Main", 2D) = "white" {}
		_MainTint("Main Tint", Color) = (1,1,1,1)
		[Toggle(_)] _ScrollMainTex("Scroll Main?", Float) = 0
		[HDR]_MainTexScaleOffset("MainTex Scale + Offset", Vector) = (1,1,0,0)
		[Header(Overlay Texture)]
		[HDR][NoScaleOffset]_Overlay("Overlay", 2D) = "white" {}
		_OverlayTint("Overlay Tint", Color) = (1,1,1,1)
		[Toggle(_)] _ScrollOverlay("Scroll Overlay?", Float) = 0
		[HDR]_OverlayScaleOffset("Overlay Scale + Offset", Vector) = (1,1,0,0)
		[Header(Overlay 2 Texture)]
		[HDR][NoScaleOffset]_Overlay2("Overlay 2", 2D) = "white" {}
		_Overlay2Tint("Overlay 2 Tint", Color) = (1,1,1,1)
		[Toggle(_)] _ScrollOverlay2("Scroll Overlay 2?", Float) = 0
		[HDR]_Overlay2ScaleOffset("Overlay 2 Scale + Offset", Vector) = (1,1,0,0)
		[Header(Main Control)]
		_Intensity("Intensity", Range( 0 , 1)) = 1
		_SkyIntensity("Sky Cover Intensity", Range( 0 , 1)) = 0
		_GrdIntensity("Ground Cover Intensity", Range( 0 , 1)) = 0
		[Header(Advanced Settings)]
		[Toggle(_)] _StereoProjection("Stereo Projection", Float) = 0
		[Toggle(_)] _InfProjection("Infinite Projection", Float) = 1
        [Enum(UnityEngine.Rendering.CullMode)] _CullMode("Cull Mode", Float) = 2                     // "Back"
		[Header(Blend Settings)]
		[Enum(UnityEngine.Rendering.BlendMode)] _SrcBlend("Source Blend", Float) = 1                 // "One"
        [Enum(UnityEngine.Rendering.BlendMode)] _DstBlend("Destination Blend", Float) = 1            // "One"
        [Enum(UnityEngine.Rendering.BlendOp)] _BlendOp("Blend Operation", Float) = 0                 // "Add"
        [Enum(UnityEngine.Rendering.ColorWriteMask)] _ColorWriteMask("Color Write Mask", Float) = 15 // "All"
		[Space]
        [Enum(UnityEngine.Rendering.CompareFunction)] _ZTest("Depth Test", Float) = 4                // "LessEqual"
        [Enum(DepthWrite)] _ZWrite("Depth Write", Float) = 0                                         // "On"
		[Header(Stencil Settings)]
        _StencilComp ("Stencil Comparison", Float) = 8
        _Stencil ("Stencil ID", Float) = 0
		[Enum(UnityEngine.Rendering.StencilOp)] _StencilOp ("Stencil Operation", Int) = 0
		[Enum(UnityEngine.Rendering.StencilOp)] _StencilFail ("Stencil Fail", Int) = 0
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilReadMask ("Stencil Read Mask", Float) = 255
	}
	
	SubShader
	{
		Tags { "RenderType"="Transparent" "Queue"="Transparent-365" }
		LOD 100
		CGINCLUDE
		#pragma target 3.0
		ENDCG
		Blend [_SrcBlend] [_DstBlend]
        BlendOp[_BlendOp]
		Cull [_CullMode]
		ColorMask [_ColorWriteMask]
		ZWrite [_ZWrite]
		ZTest [_ZTest]

        Stencil
        {
            Ref [_Stencil]
            Comp [_StencilComp]
			Pass [_StencilOp]
			Fail [_StencilFail]
            ReadMask [_StencilReadMask]
            WriteMask [_StencilWriteMask]
        }
		
		

		Pass
		{
			Name "FORWARD"
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_instancing
			#include "UnityCG.cginc"
			#include "UnityShaderVariables.cginc"


			struct appdata
			{
				float4 vertex : POSITION;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};
			
			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 texcoord : TEXCOORD0;
				UNITY_VERTEX_OUTPUT_STEREO
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			uniform sampler2D _MainTex;
			uniform float4 _MainTint;
			uniform float4 _MainTexScaleOffset;
			uniform sampler2D _Overlay;
			uniform float4 _OverlayTint;
			uniform float4 _OverlayScaleOffset;
			uniform sampler2D _Overlay2;
			uniform float4 _Overlay2Tint;
			uniform float4 _Overlay2ScaleOffset;
			uniform float _Intensity;
			uniform float _StereoProjection;
			uniform float _ScrollMainTex;
			uniform float _ScrollOverlay;
			uniform float _InfProjection;
			uniform float _SkyIntensity;
			uniform float _GrdIntensity;
			
			float2 MonoPanoProjection( float3 coords )
			{
				float3 normalizedCoords = normalize(coords);
				float latitude = acos(normalizedCoords.y);
				float longitude = atan2(normalizedCoords.z, normalizedCoords.x);
				float2 sphereCoords = float2(longitude, latitude) * float2(1.0/UNITY_PI, 1.0/UNITY_PI);
				sphereCoords = float2(1.0,1.0) - sphereCoords;
				return (sphereCoords + float4(0, 1-unity_StereoEyeIndex,1,1.0).xy) * float4(0, 1-unity_StereoEyeIndex,1,1.0).zw;
			}
			
			float2 StereoPanoProjection( float3 coords )
			{
				float3 normalizedCoords = normalize(coords);
				float latitude = acos(normalizedCoords.y);
				float longitude = atan2(normalizedCoords.z, normalizedCoords.x);
				float2 sphereCoords = float2(longitude, latitude) * float2(0.5/UNITY_PI, 1.0/UNITY_PI);
				sphereCoords = float2(0.5,1.0) - sphereCoords;
				return (sphereCoords + float4(0, 1-unity_StereoEyeIndex,1,0.5).xy) * float4(0, 1-unity_StereoEyeIndex,1,0.5).zw;
			}
			
			
			v2f vert ( appdata v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
				UNITY_TRANSFER_INSTANCE_ID(v, o);

				float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.texcoord.xyz = worldPos;
				
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.texcoord.w = 0;
				
				v.vertex.xyz +=  float3(0,0,0) ;
				o.vertex = UnityObjectToClipPos(v.vertex);

				if (_InfProjection) 
				{
					#if defined(UNITY_REVERSED_Z)
					// when using reversed-Z, make the Z be just a tiny
					// bit above 0.0
					o.vertex.z = 1.0e-9f;
					#else
					// when not using reversed-Z, make Z/W be just a tiny
					// bit below 1.0
					o.vertex.z = o.vertex.w - 1.0e-6f;
					#endif
				}
				return o;
			}
			
			fixed4 frag (v2f i ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID(i);
				fixed4 finalColor;
				float3 worldPos = i.texcoord.xyz;
				float3 worldViewDir = UnityWorldSpaceViewDir(worldPos);
				worldViewDir = normalize(worldViewDir);
				float3 invViewDir = ( worldViewDir * -1.0 );

				float2 viewDdx = ddx(invViewDir.xy);
				float2 viewDdy = ddy(invViewDir.xy);

				float2 panoUVs = _StereoProjection? StereoPanoProjection( invViewDir ) : MonoPanoProjection( invViewDir );

				float2 mainTexScroll =  (1 + _Time.y * _ScrollMainTex);
				float2 overlayScroll =  (1 + _Time.y * _ScrollOverlay);
				float2 overlay2Scroll =  (1 + _Time.y * _ScrollOverlay);

				float skyUp = (saturate(-worldViewDir.y));
				float skyDown = (saturate(worldViewDir.y));
				float4 skyCap = tex2Dgrad( _MainTex, (worldViewDir.xz / max(skyUp, skyDown))+worldViewDir.xz, viewDdx, viewDdy)
				*max(skyUp*skyUp*_SkyIntensity, skyDown*skyDown*_GrdIntensity);
				
				finalColor = ( 
					(tex2Dgrad( _MainTex,  panoUVs*_MainTexScaleOffset.xy + mainTexScroll,viewDdx,viewDdy   ) * _MainTint) *
					(tex2Dgrad( _Overlay,  panoUVs*_OverlayScaleOffset.xy + overlayScroll,viewDdx,viewDdy   ) * _OverlayTint) *
					(tex2Dgrad( _Overlay2, panoUVs*_Overlay2ScaleOffset.xy + overlay2Scroll,viewDdx,viewDdy ) * _Overlay2Tint) *
					_Intensity )+skyCap;
				return finalColor;
			}
			ENDCG
		}
	}
	
	
}